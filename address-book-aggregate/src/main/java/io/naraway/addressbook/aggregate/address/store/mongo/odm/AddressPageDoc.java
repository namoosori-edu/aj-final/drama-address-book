/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naraway.addressbook.aggregate.address.store.mongo.odm;

import io.naraway.accent.domain.ddd.Updatable;
import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;
import io.naraway.accent.store.mongo.StageEntityDoc;
import io.naraway.addressbook.aggregate.address.domain.entity.vo.GeoCoordinate;
import io.naraway.addressbook.aggregate.address.domain.entity.vo.Address;
import java.util.List;
import io.naraway.addressbook.aggregate.address.domain.entity.vo.Field;
import io.naraway.addressbook.aggregate.address.domain.entity.AddressPage;
import org.springframework.beans.BeanUtils;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@Document(collection = "ADDRESS_PAGE")
public class AddressPageDoc extends StageEntityDoc {
    //
    private GeoCoordinate geoCoordinate; // 
    private String name;
    private Address address;
    private String phoneNumber;
    private List<Field> fields; // Delivery instruction, PersonalCustomId, etc
    private boolean baseAddress;        // Default Address
    private String memo;
    private String addressBookId;

    public AddressPageDoc(AddressPage addressPage) {
        /* Autogen by nara studio */
        super(addressPage);
        BeanUtils.copyProperties(addressPage, this);
    }

    public AddressPage toDomain() {
        /* Autogen by nara studio */
        AddressPage addressPage = new AddressPage(getId(), genActorKey());
        BeanUtils.copyProperties(this, addressPage);
        return addressPage;
    }

    public static List<AddressPage> toDomains(List<AddressPageDoc> addressPageDocs) {
        /* Autogen by nara studio */
        return addressPageDocs.stream().map(AddressPageDoc::toDomain).collect(Collectors.toList());
    }

    public String toString() {
        /* Autogen by nara studio */
        return toJson();
    }

    public static AddressPageDoc sample() {
        /* Autogen by nara studio */
        return new AddressPageDoc(AddressPage.sample());
    }

    public static void main(String[] args) {
        /* Autogen by nara studio */
        System.out.println(sample());
    }
}
