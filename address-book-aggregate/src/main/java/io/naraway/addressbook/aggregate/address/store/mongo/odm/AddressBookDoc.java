/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naraway.addressbook.aggregate.address.store.mongo.odm;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;
import io.naraway.accent.store.mongo.StageEntityDoc;
import io.naraway.addressbook.aggregate.address.domain.entity.AddressBook;
import org.springframework.beans.BeanUtils;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@Document(collection = "ADDRESS_BOOK")
public class AddressBookDoc extends StageEntityDoc {
    //
    private String name; //
    private String description;

    public AddressBookDoc(AddressBook addressBook) {
        /* Autogen by nara studio */
        super(addressBook);
        BeanUtils.copyProperties(addressBook, this);
    }

    public AddressBook toDomain() {
        /* Autogen by nara studio */
        AddressBook addressBook = new AddressBook(getId(), genActorKey());
        BeanUtils.copyProperties(this, addressBook);
        return addressBook;
    }

    public static List<AddressBook> toDomains(List<AddressBookDoc> addressBookDocs) {
        /* Autogen by nara studio */
        return addressBookDocs.stream().map(AddressBookDoc::toDomain).collect(Collectors.toList());
    }

    public String toString() {
        /* Autogen by nara studio */
        return toJson();
    }

    public static AddressBookDoc sample() {
        /* Autogen by nara studio */
        return new AddressBookDoc(AddressBook.sample());
    }

    public static void main(String[] args) {
        /* Autogen by nara studio */
        System.out.println(sample());
    }
}
