package io.naraway.addressbook.aggregate;

import io.naraway.accent.domain.key.kollection.DramaRole;
import io.naraway.accent.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.util.Assert;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class AddressBookDramaRole {
    //
    public static final String Director = "director";
    public static final String Owner = "owner";

    private List<DramaRole> roles;

    public static AddressBookDramaRole fromJson(String json) {
        //
        AddressBookDramaRole addressBookDramaRole = JsonUtil.fromJson(json, AddressBookDramaRole.class);
        addressBookDramaRole.validate();
        return addressBookDramaRole;
    }

    public void validate() {
        //
        Assert.notNull(this.roles, "'roles' is required");

        if (roles.stream().noneMatch(role -> role.getCode().equals(Director))) {
            throw new IllegalArgumentException("drama role is missing, role = "+ Director);
        }
        if (roles.stream().noneMatch(role -> role.getCode().equals(Owner))) {
            throw new IllegalArgumentException("drama role is missing, role = " + Owner);
        }
    }
}