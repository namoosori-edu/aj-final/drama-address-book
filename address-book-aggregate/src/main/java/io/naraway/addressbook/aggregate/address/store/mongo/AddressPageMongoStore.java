/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naraway.addressbook.aggregate.address.store.mongo;

import io.naraway.accent.domain.type.Offset;
import io.naraway.addressbook.aggregate.address.domain.entity.AddressPage;
import io.naraway.addressbook.aggregate.address.store.AddressPageStore;
import io.naraway.addressbook.aggregate.address.store.mongo.odm.AddressPageDoc;
import io.naraway.addressbook.aggregate.address.store.mongo.repository.AddressPageMongoRepository;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class AddressPageMongoStore implements AddressPageStore {
    /* Autogen by nara studio */
    private final AddressPageMongoRepository addressPageMongoRepository;

    public AddressPageMongoStore(AddressPageMongoRepository addressPageMongoRepository) {
        /* Autogen by nara studio */
        this.addressPageMongoRepository = addressPageMongoRepository;
    }

    @Override
    public void create(AddressPage addressPage) {
        /* Autogen by nara studio */
        AddressPageDoc addressPageDoc = new AddressPageDoc(addressPage);
        addressPageMongoRepository.save(addressPageDoc);
    }

    @Override
    public void createAll(List<AddressPage> addressPages) {
        /* Autogen by nara studio */
        if (addressPages == null) {
            return;
        }
        addressPages.forEach(this::create);
    }

    @Override
    public AddressPage retrieve(String id) {
        /* Autogen by nara studio */
        Optional<AddressPageDoc> addressPageDoc = addressPageMongoRepository.findById(id);
        return addressPageDoc.map(AddressPageDoc::toDomain).orElse(null);
    }

    @Override
    public void update(AddressPage addressPage) {
        /* Autogen by nara studio */
        AddressPageDoc addressPageDoc = new AddressPageDoc(addressPage);
        addressPageMongoRepository.save(addressPageDoc);
    }

    @Override
    public void delete(AddressPage addressPage) {
        /* Autogen by nara studio */
        addressPageMongoRepository.deleteById(addressPage.getId());
    }

    @Override
    public void delete(String id) {
        /* Autogen by nara studio */
        addressPageMongoRepository.deleteById(id);
    }

    @Override
    public void deleteAll(List<String> ids) {
        /* Autogen by nara studio */
        if (ids == null) {
            return;
        }
        ids.forEach(this::delete);
    }

    @Override
    public boolean exists(String id) {
        /* Autogen by nara studio */
        return addressPageMongoRepository.existsById(id);
    }

    private Pageable createPageable(Offset offset) {
        /* Autogen by nara studio */
        if (offset.getSortDirection() != null && offset.getSortingField() != null) {
            return PageRequest.of(offset.page(), offset.limit(), (offset.ascendingSort() ? Sort.Direction.ASC : Sort.Direction.DESC), offset.getSortingField());
        } else {
            return PageRequest.of(offset.page(), offset.limit());
        }
    }

    @Override
    public Page<AddressPage> retrieveByAddressBookId(String addressBookId, Offset offest) {
        /* Autogen by nara studio */
        Page<AddressPageDoc> addressPageDocs = addressPageMongoRepository.findByAddressBookId(addressBookId, createPageable(offest));
        return new PageImpl(addressPageDocs.getContent(), addressPageDocs.getPageable(), addressPageDocs.getTotalElements());
    }

    @Override
    public List<AddressPage> retrieveByAddressBookId(String addressBookId) {
        /* Autogen by nara studio */
        return AddressPageDoc.toDomains(addressPageMongoRepository.findByAddressBookId(addressBookId));
    }

    @Override
    public AddressPage retrieveByAddressBookIdAndBaseAddress(String addressBookId, boolean baseAddress) {
        /* Autogen by nara studio */
        Optional<AddressPageDoc> addressPageDoc = addressPageMongoRepository.findByAddressBookIdAndBaseAddress(addressBookId, baseAddress);
        return addressPageDoc.map(AddressPageDoc::toDomain).orElse(null);
    }
}
