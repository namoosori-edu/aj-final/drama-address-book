package io.naraway.addressbook.facade.api.feature.personalbook.command.command;

import io.naraway.accent.domain.ddd.AuthorizedRole;
import io.naraway.accent.domain.key.tenant.CineroomKey;
import io.naraway.accent.domain.key.tenant.CitizenKey;
import io.naraway.accent.domain.trail.CommandRequest;
import io.naraway.accent.util.json.JsonUtil;
import io.naraway.addressbook.aggregate.AddressBookDramaRole;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.util.Assert;

import java.util.UUID;

@AuthorizedRole(AddressBookDramaRole.Owner)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MigratePersonalPageCommand extends CommandRequest {
    //
    private String addressPageId;
    private String teamAddressBookId;

    public void validate() {
        //
        Assert.notNull(addressPageId, "addressPageId is required.");
        Assert.notNull(teamAddressBookId, "teamAddressBookId is required.");
    }

    public static MigratePersonalPageCommand fromJson(String json) {
        //
        return JsonUtil.fromJson(json, MigratePersonalPageCommand.class);
    }

    public static MigratePersonalPageCommand sample() {
        //
        return new MigratePersonalPageCommand(
                UUID.randomUUID().toString(),
                CineroomKey.sample().getId()
        );
    }

    public static void main(String[] args) {
        //
        System.out.println(sample().toPrettyJson());
    }}
