package io.naraway.addressbook.facade.api.feature.teambook.command.command;

import io.naraway.accent.domain.ddd.AuthorizedRole;
import io.naraway.accent.domain.trail.CommandRequest;
import io.naraway.accent.util.json.JsonUtil;
import io.naraway.addressbook.aggregate.AddressBookDramaRole;
import io.naraway.addressbook.aggregate.address.domain.entity.sdo.AddressBookCdo;
import io.naraway.addressbook.facade.api.feature.personalbook.command.command.RegisterPersonalBookCommand;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.util.Assert;

@AuthorizedRole(isPublic = true)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RegisterTeamBookCommand extends CommandRequest {
    //
    private AddressBookCdo addressBookCdo;

    public void validate() {
        //
        Assert.notNull(addressBookCdo, "addressBookCdo is required.");
    }

    public String toString() {
        //
        return toJson();
    }

    public static RegisterPersonalBookCommand fromJson(String json) {
        //
        return JsonUtil.fromJson(json, RegisterPersonalBookCommand.class);
    }

    public static RegisterPersonalBookCommand sample() {
        //
        return new RegisterPersonalBookCommand(
                AddressBookCdo.teamBookSample()
        );
    }

    public static void main(String[] args) {
        //
        System.out.println(sample().toPrettyJson());
    }
}
