/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naraway.addressbook.facade.api.feature.personalbook.command.command;

import io.naraway.accent.domain.ddd.AuthorizedRole;
import io.naraway.accent.domain.key.tenant.CitizenKey;
import io.naraway.accent.domain.trail.CommandRequest;
import io.naraway.accent.util.json.JsonUtil;
import io.naraway.addressbook.aggregate.AddressBookDramaRole;
import io.naraway.addressbook.aggregate.address.domain.entity.sdo.AddressPageCdo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.util.Assert;

import java.util.UUID;


@AuthorizedRole(AddressBookDramaRole.Owner)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AssignPersonalBaseAddressCommand extends CommandRequest {
    //
    private String addressBookId;
    private String addressPageId;

    public void validate() {
        //
        Assert.notNull(addressBookId, "addressBookId is required.");
        Assert.notNull(addressPageId, "addressPageId is required.");
    }

    public String toString() {
        //
        return toJson();
    }

    public static AssignPersonalBaseAddressCommand fromJson(String json) {
        //
        return JsonUtil.fromJson(json, AssignPersonalBaseAddressCommand.class);
    }

    public static AssignPersonalBaseAddressCommand sample() {
        //
        return new AssignPersonalBaseAddressCommand(
                CitizenKey.sample().getId(),
                UUID.randomUUID().toString()
        );
    }

    public static void main(String[] args) {
        //
        System.out.println(sample().toPrettyJson());
    }
}