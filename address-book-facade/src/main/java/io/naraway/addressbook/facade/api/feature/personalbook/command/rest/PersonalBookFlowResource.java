package io.naraway.addressbook.facade.api.feature.personalbook.command.rest;

import io.naraway.accent.domain.trail.CommandResponse;
import io.naraway.accent.domain.type.NameValueList;
import io.naraway.addressbook.aggregate.address.domain.entity.sdo.AddressBookCdo;
import io.naraway.addressbook.aggregate.address.domain.entity.sdo.AddressPageCdo;
import io.naraway.addressbook.facade.api.feature.personalbook.command.command.*;
import io.naraway.addressbook.feature.personalbook.flow.PersonalBookFlow;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/feature/personalbook")
public class PersonalBookFlowResource implements PersonalBookFlowFacade {
    //
    private final PersonalBookFlow personalBookFlow;

    public PersonalBookFlowResource(PersonalBookFlow personalBookFlow) {
        //
        this.personalBookFlow = personalBookFlow;
    }

    @Override
    @PostMapping("/register-personal-book/command")
    public CommandResponse registerPersonalBook(@RequestBody RegisterPersonalBookCommand command) {
        //
        command.validate();
        AddressBookCdo addressBookCdo = command.getAddressBookCdo();

        String entityId = personalBookFlow.registerPersonalBook(addressBookCdo);
        command.setResponse(entityId);
        return command.getResponse();
    }

    @Override
    @PostMapping("/modify-personal-book/command")
    public CommandResponse modifyPersonalBook(@RequestBody ModifyPersonalBookCommand command) {
        //
        command.validate();
        String addressBookId = command.getAddressBookId();
        NameValueList nameValueList = command.getNameValueList();

        personalBookFlow.modifyPersonalBook(addressBookId, nameValueList);
        command.setResponse(addressBookId);
        return command.getResponse();
    }

    @Override
    @PostMapping("/register-personal-page/command")
    public CommandResponse registerPersonalPage(@RequestBody RegisterPersonalPageCommand command) {
        //
        command.validate();
        AddressPageCdo addressPageCdo = command.getAddressPageCdo();

        String entityId = personalBookFlow.registerPersonalPage(addressPageCdo);
        command.setResponse(entityId);
        return command.getResponse();
    }

    @Override
    @PostMapping("/modify-personal-page/command")
    public CommandResponse modifyPersonalPage(@RequestBody ModifyPersonalPageCommand command) {
        //
        command.validate();
        String addressPageId = command.getAddressPageId();
        NameValueList nameValueList = command.getNameValueList();

        personalBookFlow.modifyPersonalPage(addressPageId, nameValueList);
        command.setResponse(addressPageId);
        return command.getResponse();
    }

    @Override
    @PostMapping("/assign-personal-base-address/command")
    public CommandResponse assignPersonalBaseAddress(@RequestBody AssignPersonalBaseAddressCommand command) {
        //
        command.validate();
        String addressBookId = command.getAddressBookId();
        String addressPageId = command.getAddressPageId();

        personalBookFlow.assignPersonalBaseAddress(addressBookId, addressPageId);
        command.setResponse(addressPageId);
        return command.getResponse();
    }

    @Override
    @PostMapping("/migrate-personal-page/command")
    public CommandResponse migratePersonalPage(@RequestBody MigratePersonalPageCommand command) {
        command.validate();
        String addressPageId = command.getAddressPageId();
        String teamAddressBookId = command.getTeamAddressBookId();

        personalBookFlow.migratePersonalPage(addressPageId, teamAddressBookId);
        command.setResponse(addressPageId);
        return command.getResponse();
    }
}