package io.naraway.addressbook.facade.api.feature.teambook.command.rest;

import io.naraway.accent.domain.trail.CommandResponse;
import io.naraway.accent.domain.type.NameValueList;
import io.naraway.addressbook.aggregate.address.domain.entity.sdo.AddressBookCdo;
import io.naraway.addressbook.aggregate.address.domain.entity.sdo.AddressPageCdo;
import io.naraway.addressbook.facade.api.feature.teambook.command.command.*;
import io.naraway.addressbook.feature.personalbook.flow.PersonalBookFlow;
import io.naraway.addressbook.feature.teambook.flow.TeamBookFlow;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/feature/teambook")
public class TeamBookFlowResource implements TeamBookFlowFacade {
    //
    private final TeamBookFlow teamBookFlow;

    public TeamBookFlowResource(TeamBookFlow teamBookFlow) {
        //
        this.teamBookFlow = teamBookFlow;
    }

    @Override
    @PostMapping("/register-team-book/command")
    public CommandResponse registerTeamBook(@RequestBody RegisterTeamBookCommand command) {
        //
        command.validate();
        AddressBookCdo addressBookCdo = command.getAddressBookCdo();

        String entityId = teamBookFlow.registerTeamBook(addressBookCdo);
        command.setResponse(entityId);
        return command.getResponse();
    }

    @Override
    @PostMapping("/modify-team-book/command")
    public CommandResponse modifyTeamBook(@RequestBody ModifyTeamBookCommand command) {
        //
        command.validate();
        String addressBookId = command.getAddressBookId();
        NameValueList nameValueList = command.getNameValueList();

        teamBookFlow.modifyTeamBook(addressBookId, nameValueList);
        command.setResponse(addressBookId);
        return command.getResponse();
    }

    @Override
    @PostMapping("/register-team-page/command")
    public CommandResponse registerTeamPage(@RequestBody RegisterTeamPageCommand command) {
        //
        command.validate();
        AddressPageCdo addressPageCdo = command.getAddressPageCdo();

        String entityId = teamBookFlow.registerTeamPage(addressPageCdo);
        command.setResponse(entityId);
        return command.getResponse();
    }

    @Override
    @PostMapping("/modify-team-page/command")
    public CommandResponse modifyTeamPage(@RequestBody ModifyTeamPageCommand command) {
        //
        command.validate();
        String addressPageId = command.getAddressPageId();
        NameValueList nameValueList = command.getNameValueList();

        teamBookFlow.modifyTeamPage(addressPageId, nameValueList);
        command.setResponse(addressPageId);
        return command.getResponse();
    }

    @Override
    @PostMapping("/assign-team-page/command")
    public CommandResponse assignTeamBaseAddress(@RequestBody AssignTeamBaseAddressCommand command) {
        //
        command.validate();
        String addressBookId = command.getAddressBookId();
        String addressPageId = command.getAddressPageId();

        teamBookFlow.assignTeamBaseAddress(addressBookId, addressPageId);
        command.setResponse(addressPageId);
        return command.getResponse();
    }

    @Override
    @PostMapping("/migrate-team-page/command")
    public CommandResponse migratePersonalPage(@RequestBody MigrateTeamPageCommand command) {
        command.validate();
        String addressPageId = command.getAddressPageId();
        String personalAddressBookId = command.getPersonalAddressBookId();

        teamBookFlow.migrateTeamPage(addressPageId, personalAddressBookId);
        command.setResponse(addressPageId);
        return command.getResponse();
    }
}