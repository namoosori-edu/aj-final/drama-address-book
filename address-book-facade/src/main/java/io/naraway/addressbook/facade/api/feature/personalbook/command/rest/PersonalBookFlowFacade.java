package io.naraway.addressbook.facade.api.feature.personalbook.command.rest;

import io.naraway.accent.domain.trail.CommandResponse;
import io.naraway.addressbook.facade.api.feature.personalbook.command.command.*;

public interface PersonalBookFlowFacade {
    //
    CommandResponse registerPersonalBook(RegisterPersonalBookCommand command);
    CommandResponse modifyPersonalBook(ModifyPersonalBookCommand command);
    CommandResponse registerPersonalPage(RegisterPersonalPageCommand command);
    CommandResponse modifyPersonalPage(ModifyPersonalPageCommand command);
    CommandResponse assignPersonalBaseAddress(AssignPersonalBaseAddressCommand command);
    CommandResponse migratePersonalPage(MigratePersonalPageCommand command);
}
