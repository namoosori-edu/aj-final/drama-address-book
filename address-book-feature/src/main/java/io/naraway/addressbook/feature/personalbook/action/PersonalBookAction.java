package io.naraway.addressbook.feature.personalbook.action;

import io.naraway.addressbook.aggregate.AddressBookDramaRole;
import io.naraway.drama.prologue.spacekeeper.support.DramaRequestContext;
import io.naraway.drama.prologue.spacekeeper.support.NoIdenticalPersonException;
import io.naraway.drama.prologue.spacekeeper.support.NoSuchRoleException;
import org.springframework.stereotype.Component;

@Component
public class PersonalBookAction {
    //
    public void example(String addressBookId) {
        if (!hasRole(AddressBookDramaRole.Owner)) {
            throw new NoSuchRoleException(
                    DramaRequestContext.current().getRoles(),
                    AddressBookDramaRole.Owner
            );
        }

        // Real code
        if (!isIdenticalPerson(addressBookId)) {
            throw new NoIdenticalPersonException(
                    DramaRequestContext.current().getCitizenId(),
                    addressBookId
            );
        }
    }

    public boolean isIdenticalPerson(String citizenId) {
        //
        if (citizenId.equals(DramaRequestContext.current().getCitizenId())) {
            return true;
        }

        return false;
    }

    public boolean hasRole(String role) {
        //
        if (DramaRequestContext.current().hasRole(role)) {
            return true;
        }

        return false;
    }
}