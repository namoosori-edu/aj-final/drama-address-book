package io.naraway.addressbook.feature.personalbook.flow;

import io.naraway.accent.domain.type.Offset;
import io.naraway.addressbook.aggregate.address.domain.entity.AddressBook;
import io.naraway.addressbook.aggregate.address.domain.entity.AddressPage;
import io.naraway.addressbook.aggregate.address.domain.logic.AddressBookLogic;
import io.naraway.addressbook.aggregate.address.domain.logic.AddressPageLogic;
import io.naraway.addressbook.feature.personalbook.action.PersonalBookAction;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class PersonalBookSeek {
    //
    private final AddressBookLogic addressBookLogic;
    private final AddressPageLogic addressPageLogic;
    private final PersonalBookAction personalBookAction;

    public PersonalBookSeek(AddressPageLogic addressPageLogic,
                            AddressBookLogic addressBookLogic,
                            PersonalBookAction personalBookAction) {
        //
        this.addressBookLogic = addressBookLogic;
        this.addressPageLogic = addressPageLogic;
        this.personalBookAction = personalBookAction;
    }

    public AddressBook findPersonalBook(String addressBookId) {
        //
        return addressBookLogic.findAddressBook(addressBookId);
    }

    public List<AddressPage> findPersonalPagesByAddressBookId(String addressBookId) {
        //
        if (!personalBookAction.isIdenticalPerson(addressBookId)) {
            throw new IllegalArgumentException("No identical: requester and request info.");
        }
        return addressPageLogic.findByAddressBookId(addressBookId);

    }

    public Page<AddressPage> findPersonalPagesPagedByAddressBookId(String addressBookId, Offset offset) {
        //
        if (!personalBookAction.isIdenticalPerson(addressBookId)) {
            throw new IllegalArgumentException("No identical: requester and request info.");
        }
        return addressPageLogic.findByAddressBookId(addressBookId, offset);
    }

    public AddressPage findPersonalPage(String addressPageId) {
        //
        AddressPage addressPage = addressPageLogic.findAddressPage(addressPageId);
        if (addressPage != null) {
            if (!personalBookAction.isIdenticalPerson(addressPage.getAddressBookId())) {
                throw new IllegalArgumentException("No identical: requester and request info.");
            }
        }
        return addressPage;
    }
}