package io.naraway.addressbook.feature.teambook.action;

import io.naraway.addressbook.feature.shared.support.NoIdenticalTeamException;
import io.naraway.drama.prologue.spacekeeper.support.DramaRequestContext;
import io.naraway.drama.prologue.spacekeeper.support.NoIdenticalPersonException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

@Service
public class TeamBookAction {
    //
    public boolean isIdenticalTeam(String cineroomId) {
        //
        return !CollectionUtils.isEmpty(DramaRequestContext.current().getCineroomIds())
                && DramaRequestContext.current().getCineroomIds().contains(cineroomId);
    }

}