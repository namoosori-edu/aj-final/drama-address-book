package io.naraway.addressbook.feature.teambook.flow;

import io.naraway.accent.domain.type.Offset;
import io.naraway.addressbook.aggregate.address.domain.entity.AddressBook;
import io.naraway.addressbook.aggregate.address.domain.entity.AddressPage;
import io.naraway.addressbook.aggregate.address.domain.logic.AddressBookLogic;
import io.naraway.addressbook.aggregate.address.domain.logic.AddressPageLogic;
import io.naraway.addressbook.feature.teambook.action.TeamBookAction;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class TeamBookSeek {
    //
    private final AddressBookLogic addressBookLogic;
    private final AddressPageLogic addressPageLogic;
    private final TeamBookAction teamBookAction;
    public TeamBookSeek(AddressBookLogic addressBookLogic, AddressPageLogic addressPageLogic, TeamBookAction teamBookAction) {
        //
        this.addressBookLogic = addressBookLogic;
        this.addressPageLogic = addressPageLogic;
        this.teamBookAction = teamBookAction;
    }

    public AddressBook findTeamBook(String addressBookId) {
        //
        return addressBookLogic.findAddressBook(addressBookId);
    }

    public List<AddressPage> findTeamPagesByAddressBookId(String addressBookId) {
        //
        if (!teamBookAction.isIdenticalTeam(addressBookId)) {
            throw new IllegalArgumentException("No identical: requester and request info.");
        }
        return addressPageLogic.findByAddressBookId(addressBookId);

    }

    public Page<AddressPage> findTeamPagesPagedByAddressBookId(String addressBookId, Offset offset) {
        //
        if (!teamBookAction.isIdenticalTeam(addressBookId)) {
            throw new IllegalArgumentException("No identical: requester and request info.");
        }
        return addressPageLogic.findByAddressBookId(addressBookId, offset);
    }

    public AddressPage findTeamPage(String addressPageId) {
        //
        AddressPage addressPage = addressPageLogic.findAddressPage(addressPageId);
        if (addressPage != null) {
            if (!teamBookAction.isIdenticalTeam(addressPage.getAddressBookId())) {
                throw new IllegalArgumentException("No identical: requester and request info.");
            }
        }
        return addressPage;
    }
}
