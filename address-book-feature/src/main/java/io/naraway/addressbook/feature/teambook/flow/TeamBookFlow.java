package io.naraway.addressbook.feature.teambook.flow;

import io.naraway.accent.domain.type.NameValueList;
import io.naraway.addressbook.aggregate.address.domain.entity.AddressPage;
import io.naraway.addressbook.aggregate.address.domain.entity.sdo.AddressBookCdo;
import io.naraway.addressbook.aggregate.address.domain.entity.sdo.AddressPageCdo;
import io.naraway.addressbook.aggregate.address.domain.logic.AddressBookLogic;
import io.naraway.addressbook.aggregate.address.domain.logic.AddressPageLogic;
import io.naraway.addressbook.feature.personalbook.action.PersonalBookAction;
import io.naraway.addressbook.feature.shared.support.NoIdenticalTeamException;
import io.naraway.addressbook.feature.teambook.action.TeamBookAction;
import io.naraway.drama.prologue.spacekeeper.support.DramaRequestContext;
import io.naraway.drama.prologue.spacekeeper.support.NoIdenticalPersonException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.NoSuchElementException;

@Service
@Transactional
public class TeamBookFlow {
    //
    private final AddressBookLogic addressBookLogic;
    private final AddressPageLogic addressPageLogic;
    private final TeamBookAction teamBookAction;
    private final PersonalBookAction personalBookAction;

    public TeamBookFlow(AddressBookLogic addressBookLogic,
                        AddressPageLogic addressPageLogic,
                        TeamBookAction teamBookAction,
                        PersonalBookAction personalBookAction) {
        //
        this.addressBookLogic = addressBookLogic;
        this.addressPageLogic = addressPageLogic;
        this.teamBookAction = teamBookAction;
        this.personalBookAction = personalBookAction;
    }

    public String registerTeamBook(AddressBookCdo addressBookCdo) {
        //
        String cineroomId = addressBookCdo.getAddressBookId();           // Rule: TeamBook id should be a cineroom id
        if (!teamBookAction.isIdenticalTeam(cineroomId)) {
            throw new NoIdenticalTeamException(
                    DramaRequestContext.current().getCineroomIds(),
                    cineroomId
            );
        }

        String addressBookId = addressBookCdo.getAddressBookId();
        if (addressBookLogic.existsAddressBook(addressBookId)) {
            throw new DuplicateKeyException("AddressBook already exists: " + addressBookId);
        }

        return addressBookLogic.registerAddressBook(addressBookCdo);
    }

    public void modifyTeamBook(String addressBookId, NameValueList nameValueList) {
        //
        String cineroomId = addressBookId;
        if (!teamBookAction.isIdenticalTeam(cineroomId)) {
            throw new IllegalArgumentException("No identical: requester and request info.");
        }

        addressBookLogic.modifyAddressBook(addressBookId, nameValueList);
    }

    public String registerTeamPage(AddressPageCdo addressPageCdo) {
        //
        String cineroomId = addressPageCdo.getAddressBookId();
        if (!teamBookAction.isIdenticalTeam(cineroomId)) {
            throw new NoIdenticalTeamException(
                    DramaRequestContext.current().getCineroomIds(),
                    cineroomId
            );
        }

        return addressPageLogic.registerAddressPage(addressPageCdo);
    }

    public void modifyTeamPage(String addressPageId, NameValueList nameValueList) {
        //
        AddressPage addressPage = addressPageLogic.findAddressPage(addressPageId);
        if (addressPage != null) {
            if (!teamBookAction.isIdenticalTeam(addressPage.getAddressBookId())) {
                throw new IllegalArgumentException("No identical: requester and request info.");
            }
            addressPageLogic.modifyAddressPage(addressPageId, nameValueList);
        } else {
            throw new NoSuchElementException("AddressPage id: " + addressPageId);
        }
    }

    public void assignTeamBaseAddress(String addressBookId, String addressPageId) {
        //
        String cineroomId = addressBookId;
        if (!teamBookAction.isIdenticalTeam(cineroomId)) {
            throw new NoIdenticalPersonException(
                    DramaRequestContext.current().getCitizenId(),
                    cineroomId
            );
        }

        AddressPage originAddressPage = addressPageLogic.findByAddressBookIdAndBaseAddress(addressBookId, true);
        if (originAddressPage != null) {
            originAddressPage.setBaseAddress(false);
            addressPageLogic.modifyAddressPage(originAddressPage);
        }
        AddressPage targetAddressPage = addressPageLogic.findAddressPage(addressPageId);
        if (targetAddressPage == null) {
            throw new NoSuchElementException("AddressPage id: " + addressPageId);
        }
        targetAddressPage.setBaseAddress(true);
        addressPageLogic.modifyAddressPage(targetAddressPage);
    }

    public void migrateTeamPage(String addressPageId, String personalAddressBookId) {
        //
        AddressPage addressPage = addressPageLogic.findAddressPage(addressPageId);
        if (addressPage == null) {
            throw new NoSuchElementException("TeamAddressPage id: " + addressPageId);
        }
        if (!teamBookAction.isIdenticalTeam(addressPage.getAddressBookId())) {
            throw new IllegalArgumentException("No identical: requester and request info.");
        }

        if (!addressBookLogic.existsAddressBook(personalAddressBookId)) {
            throw new NoSuchElementException("PersonalAddressBook id: " + addressPageId);
        }
        if (!personalBookAction.isIdenticalPerson(personalAddressBookId)) {
            throw new NoIdenticalPersonException(
                    DramaRequestContext.current().getCitizenId(),
                    personalAddressBookId
            );
        }

        addressPage.setAddressBookId(personalAddressBookId);
        addressPage.setBaseAddress(false);
        addressPageLogic.modifyAddressPage(addressPage);
    }
}